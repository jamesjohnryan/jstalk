### backend


node.js
* uses an **event-driven, non-blocking I/O model**
* V8 runtime, used by Chrome
* filesystem access
* C++ interop (talk to Matt)
* isomorphic


in practicality

* PaaS (Heroku)
* Serverless framework (not architecture)
  - functions as a service  
* node 8 now LTS
* node 9 release yesterday (`async/await`)
* V8 commited to no breaking commits
