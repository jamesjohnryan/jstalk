### wrapping up


* modern package manager
* type safety
* brilliant library support
* isomorphic codebase
* decent performance


JavaScript achieved what Java failed to do